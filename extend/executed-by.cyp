LOAD CSV WITH HEADERS FROM "file:///transactions.csv" AS row
WITH row
CALL {
    WITH row
    MATCH (tx:Transaction {id: toInteger(row.TRANSACTION_ID)})
    WITH tx, row
    MATCH (c:Customer {id: toInteger(row.CUSTOMER_ID)})
    CREATE (tx)-[:EXECUTED_BY]->(c)
} IN TRANSACTIONS;
