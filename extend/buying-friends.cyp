MATCH (c:Customer)<-[:EXECUTED_BY]-(tx:Transaction)-[:EXECUTED_ON]->(t:Terminal)
WITH t, tx.type AS type, c, count(*) AS count
WHERE count > 3
WITH t, type, collect(c) AS buyingFriends
UNWIND apoc.coll.combinations(buyingFriends, 2) AS pair
WITH pair[0] AS u, pair[1] AS v
MERGE (u)-[:BUYING_FRIEND]-(v)

