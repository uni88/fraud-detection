MATCH (tx:Transaction)
CALL {
    WITH tx
    SET tx.period = apoc.coll.randomItem(["morning", "afternoon", "evening", "night"]),
        tx.type = apoc.coll.randomItem(["high-tech", "food", "clothing", "consumable", "other"])
} IN TRANSACTIONS
