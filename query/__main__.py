import argparse
import time

from neo4j import GraphDatabase


def parse_arguments():
    """Read arguments from a command line."""
    parser = argparse.ArgumentParser(description="Query data of the database")

    default_uri = "bolt://localhost:7687"
    default_database = "neo4j"
    default_username = "neo4j"
    default_password = "neo4j"
    default_k = 1
    default_c = 0

    parser.add_argument(
        "--uri",
        metavar="uri",
        help=f"Neo4j DBMS uri. Default is {default_uri}.",
        type=str,
        default=default_uri,
    )

    parser.add_argument(
        "--database",
        metavar="database",
        help=f"Database name on which to perform the import. Default is {default_database}.",
        type=str,
        default=default_database,
    )

    parser.add_argument(
        "--username",
        metavar="username",
        help=f"Username for authenticated connection to DBMS. Default is {default_username}.",
        type=str,
        default=default_username,
    )

    parser.add_argument(
        "--password",
        metavar="password",
        help=f"Password for authenticated connection to DBMS. Default is {default_password}.",
        type=str,
        default=default_password,
    )

    parser.add_argument(
        "-k",
        metavar="k",
        help=f"Degree of co-customer or buying-friend relationship for query c and e respectively. Default is {default_k}.",
        type=int,
        default=default_k,
    )

    parser.add_argument(
        "-c",
        metavar="c",
        help=f"Customer on which to run query c. Default is {default_c}.",
        type=int,
        default=default_c,
    )

    parser.add_argument(
        "--quiet",
        help="Don't print the result of the query on stdout.",
        action="store_true",
    )

    parser.add_argument(
        "query", metavar="query", help="Query to run - must be a, b, c or e.", type=str
    )

    return parser.parse_args()


def main():
    driver = GraphDatabase.driver(
        uri=args.uri, database=args.database, auth=(args.username, args.password)
    )

    with driver.session() as session:
        with open(f"query/{args.query}.cyp", "r") as file:
            query = file.read()

            if args.query == "c":
                query = query.replace("<customerId>", str(args.c))
                query = query.replace("<hops>", str(2 * args.k))

            if args.query == "e":
                query = query.replace("<hops>", str(args.k))

            start = time.time()
            result = session.run(query).values()
            end = time.time()

            if not args.quiet:
                print(result)

            print(f"📈 Query {args.query} executed in {end - start}s")

    driver.close()


if __name__ == "__main__":
    args = parse_arguments()
    main()
