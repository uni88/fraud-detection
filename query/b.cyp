MATCH (s:Semester {
    id: (candidate.date.year * 2) + (candidate.date.month / 7) - 1
})-[r:USED_DURING]-(t:Terminal)-[:EXECUTED_ON]-(candidate:Transaction)
WHERE (abs(candidate.amount - r.meanTransactionAmount) / r.meanTransactionAmount) > 0.1
RETURN t.id, collect(candidate.id) AS fraudulentTransactions
