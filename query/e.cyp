MATCH p = (u:Customer)-[:BUYING_FRIEND*<hops>]->(v:Customer)
WHERE apoc.coll.different(nodes(p))
RETURN DISTINCT u.id, v.id
