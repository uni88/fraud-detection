MATCH (t:Transaction {
    semester: (date().year * 2) + (date().month / 7)
})
RETURN t.customerId, t.date.week AS week, sum(t.amount) AS totalAmount
