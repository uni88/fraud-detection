MATCH p = (u:Customer {id: <customerId>})-[:USES*<hops>]-(v:Customer) // degree = k*2
WHERE NOT apoc.coll.containsDuplicates(nodes(p))
RETURN DISTINCT v
