import os
import argparse

from generators import add_frauds, generate_dataset

OUTPUT_DIR = "./generated-data/"


def parse_arguments():
    """Read arguments from a command line."""
    parser = argparse.ArgumentParser(description="Generate fraud detection data.")

    default_customers = 5000
    default_terminals = 10000
    default_radius = 5
    default_start_date = "2018-04-01"
    default_days = 183

    parser.add_argument(
        "--customers",
        metavar="customers",
        help=f"Number of customers to generate. Default is {default_customers}.",
        type=int,
        default=default_customers,
    )

    parser.add_argument(
        "--terminals",
        metavar="terminals",
        help=f"Number of terminals to generate. Default is {default_terminals}.",
        type=int,
        default=default_terminals,
    )

    parser.add_argument(
        "--radius",
        metavar="radius",
        help=f"Radius for a terminal to be considered reachable by a customer. Default is {default_radius}.",
        type=int,
        default=default_radius,
    )

    parser.add_argument(
        "--start-date",
        metavar="start_date",
        help=f"Start date to consider. Default is {default_start_date}.",
        type=str,
        default=default_start_date,
    )

    parser.add_argument(
        "--days",
        metavar="days",
        help=f"Number of days to consider, starting from start date. Default is {default_days}.",
        type=int,
        default=default_days,
    )

    parser.add_argument(
        "--output-dir",
        metavar="output_dir",
        help=f"Output directory for the generated data. Default is {OUTPUT_DIR}.",
        type=str,
        default=OUTPUT_DIR,
    )

    return parser.parse_args()


def generate(
    n_customers=5000,
    n_terminals=10000,
    nb_days=183,
    start_date="2018-04-01",
    r=5,
    output_dir=OUTPUT_DIR,
):
    (
        customer_profiles_table,
        terminal_profiles_table,
        transactions_df,
    ) = generate_dataset(n_customers, n_terminals, nb_days, start_date, r)

    transactions_df = add_frauds(
        customer_profiles_table, terminal_profiles_table, transactions_df
    )

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for table, name in [
        (customer_profiles_table, "customers.csv"),
        (terminal_profiles_table, "terminals.csv"),
        (transactions_df, "transactions.csv"),
    ]:
        table.to_csv(output_dir + name)


def main():
    generate(
        args.customers,
        args.terminals,
        args.days,
        args.start_date,
        args.radius,
        args.output_dir,
    )


if __name__ == "__main__":
    args = parse_arguments()
    main()
