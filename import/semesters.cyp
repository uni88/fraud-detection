MATCH (tx:Transaction)
WITH tx.date.year AS year, tx.date.month / 7 AS semester, count(*) AS count
CREATE (s:Semester { id: (year * 2) + semester});
