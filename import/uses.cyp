LOAD CSV FROM "file:///customers.csv" AS row
WITH row
CALL {
    WITH row
    MATCH (c:Customer {id: toInteger(row[1])})
    WITH c, row
    UNWIND split(replace(replace(row[7], '[', ''), ']', ''), ', ') AS terminalId
    MATCH (t:Terminal {id: toInteger(terminalId)})
    CREATE (c)-[:USES]->(t)
} IN TRANSACTIONS
