LOAD CSV WITH HEADERS FROM "file:///customers.csv" AS row
WITH row WHERE row.CUSTOMER_ID IS NOT NULL
CREATE (c:Customer {
    id: toInteger(row.CUSTOMER_ID),
    x: toFloat(row.x_customer_id),
    y: toFloat(row.y_customer_id),
    meanTransactionAmount: toInteger(row.mean_amount),
    transactionsPerDay: toFloat(row.mean_nb_tx_per_day)
});
