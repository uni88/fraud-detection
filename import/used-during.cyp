MATCH (t:Terminal)<-[:EXECUTED_ON]-(tx:Transaction)
WITH t, tx.semester AS semester, avg(tx.amount) AS meanTransactionAmount
MATCH (s:Semester {id: semester})
CREATE (t)-[:USED_DURING {meanTransactionAmount: meanTransactionAmount}]->(s);
