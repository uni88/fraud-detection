import argparse
import time

from neo4j import GraphDatabase


def parse_arguments():
    """Read arguments from a command line."""
    parser = argparse.ArgumentParser(description="Import data into the database")

    default_uri = "bolt://localhost:7687"
    default_database = "neo4j"
    default_username = "neo4j"
    default_password = "neo4j"

    parser.add_argument(
        "--uri",
        metavar="uri",
        help=f"Neo4j DBMS uri. Default is {default_uri}.",
        type=str,
        default=default_uri,
    )

    parser.add_argument(
        "--database",
        metavar="database",
        help=f"Database name on which to perform the import. Default is {default_database}.",
        type=str,
        default=default_database,
    )

    parser.add_argument(
        "--username",
        metavar="username",
        help=f"Username for authenticated connection to DBMS. Default is {default_username}.",
        type=str,
        default=default_username,
    )

    parser.add_argument(
        "--password",
        metavar="password",
        help=f"Password for authenticated connection to DBMS. Default is {default_password}.",
        type=str,
        default=default_password,
    )

    return parser.parse_args()


def main():
    def run_import(driver, filename):
        with driver.session() as session:
            with open(filename, "r") as file:
                print(f"🚚 Importing {filename}...")
                start = time.time()
                session.run(file.read())
                end = time.time()
                print(f"✅ Imported {filename} in {end - start}s")

            driver.close()

    driver = GraphDatabase.driver(
        uri=args.uri, database=args.database, auth=(args.username, args.password)
    )

    start = time.time()

    # 🚨 Order is relevant.
    for filename in (
        "import/customers.cyp",
        "import/terminals.cyp",
        "import/transactions.cyp",
        "import/semesters.cyp",
        "import/customer-id-index.cyp",
        "import/terminal-id-index.cyp",
        "import/transaction-id-index.cyp",
        "import/semester-id-index.cyp",
        "import/transaction-semester-index.cyp",
        "import/executed-on.cyp",
        "import/uses.cyp",
        "import/used-during.cyp",
    ):
        run_import(driver, filename)

    end = time.time()
    print(f"🎉 Import completed in {end - start}s")


if __name__ == "__main__":
    args = parse_arguments()
    main()
