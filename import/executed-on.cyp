LOAD CSV WITH HEADERS FROM "file:///transactions.csv" AS row
WITH row
CALL {
    WITH row
    MATCH (tx:Transaction {id: toInteger(row.TRANSACTION_ID)})
    WITH tx, row
    MATCH (t:Terminal {id: toInteger(row.TERMINAL_ID)})
    CREATE (tx)-[:EXECUTED_ON]->(t)
} IN TRANSACTIONS;
