LOAD CSV WITH HEADERS FROM "file:///transactions.csv" AS row
WITH row, date(split(row.TX_DATETIME, ' ')[0]) AS date
CALL {
    WITH row, date
    CREATE (tx:Transaction {
        id: toInteger(row.TRANSACTION_ID),
        date: date,
        semester: (date.year * 2) + (date.month / 7),
        amount: toFloat(row.TX_AMOUNT),
        customerId: toInteger(row.CUSTOMER_ID),
        isFraudulent: toBoolean(toInteger(row.TX_FRAUD))
    })
} IN TRANSACTIONS;
