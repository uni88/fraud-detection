LOAD CSV WITH HEADERS FROM "file:///terminals.csv" AS row
WITH row WHERE row.TERMINAL_ID IS NOT NULL
CREATE (t:Terminal {
    id: toInteger(row.TERMINAL_ID),
    x: toFloat(row.x_terminal_id),
    y: toFloat(row.y_terminal_id)
});
