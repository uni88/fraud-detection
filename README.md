# Fraud Detection

Implementation of the project for New Generation Data Models and DBMSs course.
For the documentation part, see this project's
[wiki](https://gitlab.com/uni88/fraud-detection/-/wikis/home).
